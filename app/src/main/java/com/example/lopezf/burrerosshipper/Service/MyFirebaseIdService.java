package com.example.lopezf.burrerosshipper.Service;

import com.example.lopezf.burrerosshipper.Common.Common;
import com.example.lopezf.burrerosshipper.Model.Token;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by lopezf on 4/19/2018.
 */

public class MyFirebaseIdService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        if (Common.currentShipper != null)
            updateToServer(refreshedToken);


    }

    private void updateToServer(String refreshedToken) {
        //copy code from client app
        if (Common.currentShipper != null) {
            FirebaseDatabase db = FirebaseDatabase.getInstance();
            DatabaseReference tokens = db.getReference("Tokens");
            Token data = new Token(refreshedToken, true);
            tokens.child(Common.currentShipper.getPhone()).setValue(data);
        }

    }
}
