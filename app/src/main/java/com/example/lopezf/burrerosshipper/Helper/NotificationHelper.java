package com.example.lopezf.burrerosshipper.Helper;

import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.net.Uri;
import android.os.Build;

import com.example.lopezf.burrerosshipper.R;

/**
 * Created by lopezf on 4/19/2018.
 */

public class NotificationHelper extends ContextWrapper {
    private static final String EDMT_CHANEL_ID = "com.example.lopezf.burrerosshipper.EDMTDev";
    private static final String EDMT_CHANEL_NAME = "Burreros";

    private NotificationManager manager;

    public NotificationHelper(Context base){
        super(base);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            createChannel();

    }

    private void createChannel() {
        NotificationChannel edmtChannel = new NotificationChannel(EDMT_CHANEL_ID,
                EDMT_CHANEL_NAME,NotificationManager.IMPORTANCE_DEFAULT);
        edmtChannel.enableLights(false);
        edmtChannel.enableVibration(true);
        edmtChannel.setLockscreenVisibility(android.app.Notification.VISIBILITY_PRIVATE);

        getManager().createNotificationChannel(edmtChannel);
    }

    public NotificationManager getManager() {
        if (manager == null)
            manager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        return manager;
    }
    @TargetApi(Build.VERSION_CODES.O)
    public android.app.Notification.Builder getBurrerosChannelNotification(String title, String body, PendingIntent contentIntent, Uri soundUri)
    {
        return new android.app.Notification.Builder(getApplicationContext(),EDMT_CHANEL_ID)
                .setContentIntent(contentIntent)
                .setContentTitle(title)
                .setContentText(body)
                .setSmallIcon(R.drawable.ic_local_shipping_black_24dp)
                .setSound(soundUri)
                .setAutoCancel(false);
    }

    @TargetApi(Build.VERSION_CODES.O)
    public android.app.Notification.Builder getBurrerosChannelNotification(String title, String body, Uri soundUri)
    {
        return new android.app.Notification.Builder(getApplicationContext(),EDMT_CHANEL_ID)
                .setContentTitle(title)
                .setContentText(body)
                .setSmallIcon(R.drawable.ic_local_shipping_black_24dp)
                .setSound(soundUri)
                .setAutoCancel(false);
    }
}